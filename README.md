# Postgres prometheus exporter

## Requires

* Postgres user already setup
* Set ``postgres_username`` if it differs from the default (postgres)
* Set ``postgres_group`` if it differs from the default (``postgres_username``)
